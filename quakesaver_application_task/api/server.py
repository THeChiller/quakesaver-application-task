# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Server to provide data from database."""
import logging
from inspect import getmembers, isfunction

from flask import Flask, jsonify

from quakesaver_application_task.api import statistics
from quakesaver_application_task.core import util
from quakesaver_application_task.db import DB

flask_app = Flask(__name__)


@flask_app.route("/<object_>/<attribute>/<statistic>", methods=["GET"])
def index(object_: str, attribute: str, statistic):
    """Return data by dates."""
    logging.debug(f"database: {flask_app.config.get('DB')}")
    logging.debug(
        f"Server received request:\n"
        f"                                                   object_: {object_}\n"
        f"                                                   attribute: {attribute}\n"
        f"                                                   statistic: {statistic}"
    )

    # load available statistics and db
    functions = [o for o in getmembers(statistics) if isfunction(o[1])]
    function_names = set(name for name, ref in functions if name[0] != "_")
    db: DB = flask_app.config.get("DB")

    # run selected statistic(s)
    if statistic != "all":
        for name, ref in functions:
            if name[0] != "_" and name == statistic:
                data = ref(db=db, object_=object_, attribute=attribute)
                break
        else:
            data = {
                "ERROR": f"{statistic} is not implemented yet.",
                "SOLUTION": f"Chose from {function_names}.",
            }
    else:
        data = {}
        for name, ref in functions:
            if name[0] != "_":
                data_slice = ref(db=db, object_=object_, attribute=attribute)
                if "ERROR" in data_slice:
                    data = data_slice
                    break
                else:
                    data = util.merge_dict(data, data_slice)

    resp = jsonify(data)
    resp.status_code = 200
    return resp
