# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Provide statistical attributes of data."""
import statistics
from typing import Dict

from quakesaver_application_task.api.data import _get_data
from quakesaver_application_task.db import DB


def mean(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the mean of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if "ERROR" in values:
        return values
    mean_ = statistics.mean(values)

    return {object_: {attribute: {"mean": f"{mean_}{unit}"}}}


def min(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the min of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if "ERROR" in values:
        return values

    values.sort()
    min_ = values[0]

    return {object_: {attribute: {"min": f"{min_}{unit}"}}}


def max(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the max of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if isinstance(values, dict):
        return values

    values.sort()
    max_ = values[-1]

    return {object_: {attribute: {"max": f"{max_}{unit}"}}}


def median(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the median of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if isinstance(values, dict):
        return values

    median_ = statistics.median(values)

    return {object_: {attribute: {"median": f"{median_}{unit}"}}}


def stdev(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the median of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if isinstance(values, dict):
        return values

    stdev_ = statistics.stdev(values)

    return {object_: {attribute: {"stdev": f"{stdev_}{unit}"}}}


def variance(db: DB, object_: str, attribute: str) -> Dict:
    """Calculate the median of an attribute of an object."""
    values, unit = _get_data(db=db, object_=object_, attribute=attribute)

    if isinstance(values, dict):
        return values

    variance_ = statistics.variance(values)

    return {object_: {attribute: {"variance": f"{variance_}{unit}"}}}
