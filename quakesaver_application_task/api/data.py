# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Provide statistical attributes of data."""
from typing import Dict, List, Union

from quakesaver_application_task.db import DB


def _parse_data(data: Dict, attribute: str) -> (Dict, str):
    """Separate numerical value from unit symbol."""
    return_data = {}
    return_symbol = ""

    if attribute == "price":
        return_symbol = "$"
        for object_ in data:
            return_data[object_] = {}
            for timestamp in data[object_]:
                value: str = data[object_][timestamp][attribute]
                value: float = float(value.replace("$", "").replace(",", ""))
                return_data[object_][timestamp] = {attribute: value}
    elif attribute == "weight":
        return_symbol = "kg"
        for object_ in data:
            return_data[object_] = {}
            for timestamp in data[object_]:
                value: str = data[object_][timestamp][attribute]
                value: float = float(value.replace("kg", "").replace(",", ""))
                return_data[object_][timestamp] = {attribute: value}
    elif attribute == "count":
        return_symbol = ""
        for object_ in data:
            return_data[object_] = {}
            for timestamp in data[object_]:
                value: str = data[object_][timestamp][attribute]
                value: int = int(value.replace(",", ""))
                return_data[object_][timestamp] = {attribute: value}

    return return_data, return_symbol


def _get_data(db: DB, object_: str, attribute: str) -> (Union[Dict, List], str):
    """Load specified data from db."""
    data = db.read(object_)
    return_data = {object_: {}}
    attributes = set()
    if object_ not in data:
        return {
            "ERROR": f"No data for {object_} in db.",
            "SOLUTION": f"Download data first with "
            f"`quakesaver-application-task download -p 100 {object_}`",
        }, None
    else:
        for timestamp in data[object_]:
            attributes.update(data[object_][timestamp].keys())
            if attribute in data[object_][timestamp]:
                value = data[object_][timestamp][attribute]
                return_data[object_][timestamp] = {attribute: value}

    if len(return_data[object_]) < 1:
        return {
            "ERROR": f"No data for {attribute} of {object_} in db.",
            "SOLUTION": f"Chose one of {attributes}",
        }, None

    parsed_data, unit = _parse_data(return_data, attribute)

    values = []
    # flatten dict
    for object_ in parsed_data:
        for timestamp in parsed_data[object_]:
            value: float = parsed_data[object_][timestamp][attribute]
            values.append(value)

    return values, unit
