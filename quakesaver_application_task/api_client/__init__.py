# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Provide a functions to handle requesting and saving data."""
import logging

import requests
from click import progressbar

from quakesaver_application_task.core import Settings
from quakesaver_application_task.data_source import use_server
from quakesaver_application_task.db import DB, get_db


@use_server
def download_data(settings: Settings, attribute: str) -> None:
    """Request data from mocked API and save it to db."""
    db: DB = get_db(settings=settings)
    logging.debug(f"database: {db.__class__}")

    with progressbar(settings.OBJECTS) as bar:
        for object_ in bar:
            logging.info(f"requesting data for {object_}")
            url = f"http://localhost:{settings.PORT}/{object_}/{attribute}"
            headers = {
                "from_date": str(settings.FROM_DATE),
                "to_date": str(settings.TO_DATE),
            }
            response = requests.get(url, headers=headers)

            logging.info(f"saving data for {object_}")
            db.update(data=response.json())
