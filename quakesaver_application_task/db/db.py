# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Provides db classes to handle CRUD."""
import logging
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict

import yaml

from quakesaver_application_task.core import Settings


class DB(ABC):
    """Skeleton class for db's."""

    def __init__(self, settings: Settings):
        """Create an instance of DB."""
        self.settings: Settings = settings
        self.data: Dict = {}

    @abstractmethod
    def _connect(self) -> None:
        """Establish a connection to the db."""
        pass

    @abstractmethod
    def _close(self) -> None:
        """Close a connection to the db."""
        pass

    def create(self) -> Dict:
        """Create an database entry."""
        raise NotImplementedError

    @abstractmethod
    def read(
        self, object_: str, from_date: datetime = None, to_date: datetime = None
    ) -> Dict:
        """Read an database entry."""
        pass

    @abstractmethod
    def update(self, data: Dict) -> None:
        """Update an database entry."""
        pass

    def delete(self) -> Dict:
        """Delete an database entry."""
        raise NotImplementedError


class Yml_DB(DB):
    """A yml file as db."""

    def _connect(self) -> None:
        """Establish a connection to the db."""
        if Path(self.settings.DB_YML_LOCATION).exists():
            with open(self.settings.DB_YML_LOCATION, "r") as yml_file:
                self.data = yaml.safe_load(yml_file)
        else:
            logging.warning("Connection to yml db failed. Creating a new one.")
        logging.debug(f"loaded data: {self.data}")

    def _close(self) -> None:
        """Close a connection to the db."""
        logging.info("writing to db")
        with open(self.settings.DB_YML_LOCATION, "w") as yml_file:
            yaml.safe_dump(self.data, yml_file)

    def read(
        self, object_: str, from_date: datetime = None, to_date: datetime = None
    ) -> Dict:
        """Read an database entry."""
        if to_date and from_date:
            self._connect()
            return_data = {}
            if object_ in self.data:
                return_data[object_] = {}

                current_date = from_date
                td = timedelta(hours=1)
                while current_date <= to_date:
                    if current_date in self.data[object_]:
                        return_data[object_][current_date] = self.data[object_][
                            current_date
                        ].copy()
                    current_date += td
            return return_data
        elif not to_date and not from_date:
            self._connect()
            return_data = {}
            if object_ in self.data:
                return_data[object_] = self.data[object_].copy()
            return return_data
        else:
            raise ValueError

    def update(self, data: Dict) -> None:
        """Update an database entry."""
        self._connect()
        for object_ in data:
            if object_ not in self.data:
                self.data[object_] = {}
            for time in data[object_]:
                self.data[object_][time] = data[object_][time]
        self._close()
