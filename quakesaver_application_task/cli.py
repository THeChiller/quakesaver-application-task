#!/usr/bin/env python

# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
This is the entry point for the command-line interface (CLI) application.

Parts of the code come from https://gitlab.hzdr.de/hifis/surveys/hifis-surveyval
"""
import logging
import time
from datetime import datetime
from multiprocessing import Process
from typing import Set, Tuple

import click
import pkg_resources
from click_option_group import RequiredMutuallyExclusiveOptionGroup, optgroup

from quakesaver_application_task import api_client
from quakesaver_application_task.api.server import flask_app
from quakesaver_application_task.core import Settings, set_verbosity
from quakesaver_application_task.db import get_db

settings = Settings()


@click.group()
@click.option(
    "-v",
    count=True,
    default=0,
    show_default=True,
    help="Enable verbose output. "
    "Increase verbosity by setting this option up to 3 times.",
)
@click.option(
    "-p",
    "--port",
    type=click.IntRange(0, 65535),
    default=8080,
    show_default=True,
    help="Select the port to bind servers to.",
)
@click.option(
    "-db",
    "--database_driver",
    type=click.Choice(settings.IMPLEMENTED_DB_DRIVERS, case_sensitive=False),
    default="yml",
    show_default=True,
    help="Select the database to use. The selected db driver needs to be installed",
)
def cli(
    v: int,
    port: int,
    database_driver: str,
) -> None:
    """
    Solve the task of the quakesaver application process.

    Either create a dummy server, generate dummy data and download the dummy data,
    or server the loaded data from a db to an API.
    """
    logging_level = set_verbosity(v)

    logging.debug(f"Parameter verbose: {v}")
    logging.debug(f"Parameter port: {port}")
    logging.debug(f"Parameter database_driver: {database_driver}")

    if not logging_level == logging.ERROR:
        click.echo(
            click.style(
                f"Verbose logging is enabled. "
                f"(LEVEL={logging.getLogger().getEffectiveLevel()})",
                fg="yellow",
            )
        )

    settings.PORT = port
    settings.DB_DRIVER = database_driver


@cli.command()
def version() -> None:
    """Get the library version."""
    pkg_version = pkg_resources.require("quakesaver_application_task")[0].version
    click.echo(click.style(f"{pkg_version}", bold=True))


@cli.command()
@click.option(
    "-a",
    "--attribute",
    type=click.Choice(["price", "count", "weight", "all"], case_sensitive=False),
    default="all",
    show_default=True,
    help="Select the attribute to receive data from.",
)
@optgroup.group(
    "Set time range.",
    cls=RequiredMutuallyExclusiveOptionGroup,
    help="Chose the time to download the data from. Maximum 1000 days possible.",
)
@optgroup.option(
    "-d",
    "--date_range",
    nargs=2,
    type=click.DateTime(formats=["%Y-%m-%d"]),
    help="Select a start and an end date.",
)
@optgroup.option(
    "-p", "--past_days", type=click.INT, help="Select the past <n> days from today."
)
@click.argument("object1", type=click.STRING, required=True)
@click.argument("object2", type=click.STRING, required=False)
@click.argument("object3", type=click.STRING, required=False)
def download(
    date_range: Tuple[datetime],
    past_days: int,
    attribute: str,
    object1: str,
    object2: str,
    object3: str,
) -> None:
    """
    Download mocked timeseries data from a mocked server for a given object type..

    The minimum timespan is 1 day.
    The maximum timespan is 1000 days.
    Maximum 3 objects can be handled at once.
    """
    objects: Set = {object1, object2, object3}
    objects.discard(None)

    logging.debug(f"Parameter attribute: {attribute}")
    logging.debug(f"Parameter date_range: {date_range}")
    logging.debug(f"Parameter past_days: {past_days}")
    logging.debug(f"Argument objects: {objects}")

    from_date, to_date = None, None
    if date_range:
        time1, time2 = date_range
        if time1 < time2:
            from_date, to_date = time1, time2
        else:
            from_date, to_date = time2, time1

    settings.set_time_range(from_date=from_date, to_date=to_date, past_days=past_days)
    settings.OBJECTS = objects
    debug = logging.getLogger().getEffectiveLevel() == logging.DEBUG
    api_client.download_data(
        debug=debug, port=settings.PORT, settings=settings, attribute=attribute
    )


@cli.command()
@click.option(
    "-db",
    "--database_driver",
    type=click.Choice(settings.IMPLEMENTED_DB_DRIVERS, case_sensitive=False),
    default="yml",
    show_default=True,
    help="Select the database to use. The selected db driver needs to be installed",
)
@click.argument("minutes", type=click.INT)
def serve(
    database_driver: str,
    minutes: int,
) -> None:
    """
    Start a local webserer, to server the db as REST API for a limited amount of time.

    The minimum time is 1 minute.
    If time is set to -1, it serves forver.
    """
    logging.debug(f"Parameter database_driver: {database_driver}")
    logging.debug(f"Argument minutes: {minutes}")

    # set up db
    db = get_db(settings=settings)
    logging.debug(f"db selected: {db}")

    # set up server
    debug = logging.getLogger().getEffectiveLevel() == logging.DEBUG
    flask_app.config.update(
        DEBUG=debug, SERVER_NAME=f"localhost:{settings.PORT}", DB=db
    )
    server = Process(target=flask_app.run)

    # starting server
    logging.info("starting webserver")
    server.start()
    click.echo(
        f"Run `curl -i http://localhost:{settings.PORT}/OBJECT/ATTRIBUTE/STATISTIC`"
    )

    # wait defined number of time
    seconds_to_wait = minutes * 60
    with click.progressbar(range(seconds_to_wait)) as bar:
        for second in bar:
            if server.is_alive():
                time.sleep(1)

    if server.is_alive():
        logging.info("stopping webserver")
        server.terminate()
