# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generate fake data."""
from datetime import datetime, timedelta
from typing import Dict

from faker import Faker
from faker.providers import BaseProvider, currency


def generate_fake_data(
    from_date: datetime, to_date: datetime, object_: str, attribute: str
) -> Dict:
    """Generate fake prices of a certain object in a time range."""
    data = {object_: {}}
    current_date = from_date
    td = timedelta(hours=1)
    fake = Faker()
    fake.add_provider(currency)
    fake.add_provider(BaseProvider)

    if attribute == "price":
        while current_date <= to_date:
            data[object_][current_date.__str__()] = {}
            data[object_][current_date.__str__()]["price"] = fake.pricetag()
            current_date += td
    elif attribute == "count":
        while current_date <= to_date:
            data[object_][current_date.__str__()] = {}
            data[object_][current_date.__str__()]["count"] = fake.random_int(
                min=0, max=1000000, step=1
            )
            current_date += td
    elif attribute == "weight":
        while current_date <= to_date:
            data[object_][current_date.__str__()] = {}
            data[object_][current_date.__str__()][
                "weight"
            ] = f"{fake.random_int(min=0, max=1000000, step=1)}kg"
            current_date += td
    elif attribute == "all":
        while current_date <= to_date:
            data[object_][current_date.__str__()] = {}
            data[object_][current_date.__str__()]["price"] = fake.pricetag()
            data[object_][current_date.__str__()]["count"] = fake.random_int(
                min=0, max=1000000, step=1
            )
            data[object_][current_date.__str__()][
                "weight"
            ] = f"{fake.random_int(min=0, max=1000000, step=1)}kg"
            current_date += td

    return data
