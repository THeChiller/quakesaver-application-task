# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Expose server decorator."""
import functools
import logging
from multiprocessing import Process
from typing import Any

from quakesaver_application_task.data_source.server import flask_app


def use_server(func):
    """Handle starting and stopping the fake data server."""

    @functools.wraps(func)
    def handle_server(port: int, debug: bool, **kwargs) -> Any:
        """Start the data server for the function call. End it afterwards."""
        flask_app.config.update(
            DEBUG=debug,
            SERVER_NAME=f"localhost:{port}",
        )
        server = Process(target=flask_app.run)
        logging.info("starting webserver")
        server.start()

        data = func(**kwargs)

        if server.is_alive():
            logging.info("stopping webserver")
            server.terminate()

        return data

    return handle_server
