# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Server to provide mocked metadata."""
import logging
from datetime import datetime

from flask import Flask, jsonify, request

from quakesaver_application_task.data_source import data

flask_app = Flask(__name__)


@flask_app.route("/<object_>/<attribute>", methods=["GET"])
def index(object_: str, attribute: str):
    """Return mocked data by dates."""
    from_date: datetime = datetime.strptime(
        request.headers.get("from_date"), "%Y-%m-%d %H:%M:%S"
    )
    to_date: datetime = datetime.strptime(
        request.headers.get("to_date"), "%Y-%m-%d %H:%M:%S"
    )
    logging.debug(
        f"Server received request:\n"
        f"                                                   object type: {object_}\n"
        f"                                                   from_date: {from_date}\n"
        f"                                                   to_date: {to_date}"
    )
    fake_data: dict = data.generate_fake_data(
        from_date=from_date, to_date=to_date, object_=object_, attribute=attribute
    )

    resp = jsonify(fake_data)
    resp.status_code = 200
    return resp
