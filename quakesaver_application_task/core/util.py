# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
This module provides various utility functionalities.

The parts of the code comes from https://gitlab.hzdr.de/hifis/surveys/hifis-surveyval.
"""


import logging
from typing import List


def set_verbosity(verbose_count: int) -> int:
    """Interpret the verbosity option count."""
    verbosity_options: List[int] = [
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
    ]

    max_index: int = len(verbosity_options) - 1

    # Clamp verbose_count to accepted values
    # Note that it shall not be possible to unset the verbosity.
    option_index: int = (
        0
        if verbose_count < 0
        else max_index
        if verbose_count > max_index
        else verbose_count
    )

    new_level: int = verbosity_options[option_index]

    logging.basicConfig(
        level=new_level,
        format="%(asctime)s "
        "[%(levelname)-8s] "
        "%(module)s.%(funcName)s(): "
        "%(message)s",
    )

    return new_level


# from https://stackoverflow.com/questions/7204805/\
#      how-to-merge-dictionaries-of-dictionaries
def merge_dict(a, b, path=None):
    """Merge dict b into dict a."""
    if path is None:
        path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dict(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                raise Exception("Conflict at %s" % ".".join(path + [str(key)]))
        else:
            a[key] = b[key]
    return a
