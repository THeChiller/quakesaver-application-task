# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""This module handles settings."""
import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Set, Union

import click
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings, that are not loaded from file."""

    # date to start downloading from
    FROM_DATE: datetime = None

    # date to download until
    TO_DATE: datetime = None

    # db settings
    IMPLEMENTED_DB_DRIVERS: Set = {"yml"}
    DB_DRIVER: str = "yml"

    # db driver yaml settings
    DB_YML_LOCATION: Path = Path("database.yml")

    # objects to get data from
    OBJECTS: Set[str] = None

    # port to bind servers to
    PORT: int = None

    def set_time_range(
        self,
        from_date: Union[datetime, None],
        to_date: Union[datetime, None],
        past_days: Union[int, None],
    ):
        """Determine and set the time range to download data from."""
        if past_days:
            timespan = past_days
            td = timedelta(timespan)
            to_date = datetime.now().replace(microsecond=0, second=0, minute=0)
            from_date = to_date - td
        else:
            timespan = abs(from_date - to_date).days

        logging.debug(f"timespan: {timespan}")
        logging.debug(f"to_date: {to_date}")
        logging.debug(f"from_date: {from_date}")

        if timespan > 1000:
            raise click.exceptions.BadOptionUsage(
                "date_range", "The maximum time range is 1000 days."
            )
        elif timespan < 1:
            raise click.exceptions.BadOptionUsage(
                "date_range", "The minimum time range is 1 day."
            )

        self.FROM_DATE = from_date
        self.TO_DATE = to_date
