<!--
quakesaver-application-task
This project solves the challenge provided by [quakesaver](https://quakesaver.net/).

SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

# Challenge

1. Build a CLI tool that downloads timeseries data, for example of a given stock for a given time range.
   Calling that tool might look like this:

   ```
   my_tool download 2020-01-01 2020-02-01 APPLE
   ```

That command could download stock data of Apple of February 2020.
However, the source and type of data is up to you.

2. Store this data on your computer in a simple database of your choice.
3. Add a second command that starts a small web server. e.g.:

   ```
   my_tool serve
   ```

   That web server serves from the database in a structured format (JSON, YAML, or alike) a basic data analysis.
   For example the mean of the downloaded time history, the number of data points, or the peak to peak amplitude.
   Such a request using curl could for example look like:

   ```
   curl  localhost:8080/mean --data “symbol=APPLE"
   ```

Realize the project in Python and using git.
You are free to chose libraries and tools that you consider useful as well as the time you need to come up with a minimal viable solution.
