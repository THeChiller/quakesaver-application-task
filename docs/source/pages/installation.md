<!--
quakesaver-application-task
This project solves the challenge provided by [quakesaver](https://quakesaver.net/).

SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

# Installation

## From GitLab Package Registry
```shell
pip install quakesaver-application-task --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/26299399/packages/pypi/simple
```

## From Source
```shell
git clone https://gitlab.com:THeChiller/quakesaver-application-task.git
poetry install
```
