.. quakesaver-application-task
.. This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
..
.. SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
..
.. SPDX-License-Identifier: GPL-3.0-or-later
..
.. This program is free software: you can redistribute it and/or modify
.. it under the terms of the GNU General Public License as published by
.. the Free Software Foundation, either version 3 of the License, or
.. (at your option) any later version.
..
.. This program is distributed in the hope that it will be useful,
.. but WITHOUT ANY WARRANTY; without even the implied warranty of
.. MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. GNU General Public License for more details.
..
.. You should have received a copy of the GNU General Public License
.. along with this program. If not, see <http://www.gnu.org/licenses/>.

.. quakesaver-application-task documentation master file, created by
   sphinx-quickstart on Fri Apr 30 18:23:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to quakesaver-application-task's documentation!
=======================================================

This package is designed to solve the tasks prom the challenge.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   pages/programming-challenge.md
   pages/installation.md
   pages/usage.rst
   Quality Report <https://gitlab.com/THeChiller/quakesaver-application-task/-/blob/master/QUALITY.md>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

