<!--
quakesaver-application-task
This project solves the challenge provided by [quakesaver](https://quakesaver.net/).

SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

# quakesaver application task

This project solves the [challenge](docs/source/pages/programming-challenge.md) provided by [quakesaver](https://quakesaver.net/).

## Documentation

You can find the documentation [here](https://thechiller.gitlab.io/quakesaver-application-task/).
In case you received a local copy of the project, you can build and view the documentation like this:
```shell script
make docs
$BROWSER docs/build/html/index.html
```

## License

Copyright © 2021 Maximilian Dolling <maximilian.dolling@posteo.de>

This work is licensed under the following license(s):
* Everything else is licensed under [GPL-3.0-or-later](LICENSES/GPL-3.0-or-later.txt)

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).