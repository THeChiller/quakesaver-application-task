# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.DEFAULT_GOAL := all
.PHONY: reformat lint test docs clean all
PROJ_SLUG = quakesaver_application_task
CLI_NAME = quakesaver-application-task
LINTER = flakehell lint
SHELL = bash

reformat:
	poetry run isort .
	poetry run black .

lint:
	poetry run $(LINTER) $(PROJ_SLUG)

test:
	poetry run py.test --cov-report term --cov=$(PROJ_SLUG) tests/

docs:
	poetry run sphinx-apidoc -o ./docs/source quakesaver_application_task
	cd docs && poetry run make html

clean:
	rm -rf dist \
	rm -rf tests/__pycache__ \
	rm -rf docs/build \
	rm -rf docs/source/modules.rst \
	rm -rf docs/source/pages/QUALITY.md \
	rm -rf docs/source/quakesaver_application_task.* \
	rm -rf *.egg-info \
	rm -rf .pytest_cache \
	rm -rf .reuse \
	rm -rf meta \
	rm -rf database.yml \
	rm -rf .coverage

all: clean reformat lint test
