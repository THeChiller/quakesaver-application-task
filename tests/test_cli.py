#!/usr/bin/env python

# quakesaver-application-task
# This project solves the challenge provided by [quakesaver](https://quakesaver.net/).
#
# SPDX-FileCopyrightText: 2021 HIFIS Software <support@hifis.net>
# SPDX-FileCopyrightText: 2021 Maximilian Dolling <maximilian.dolling@posteo.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

"""This is the test module for the project's command-line interface (CLI) module."""
import pkg_resources
import pytest
from click.testing import CliRunner, Result

from quakesaver_application_task import cli

# To learn more about testing Click applications, visit the link below.
# http://click.pocoo.org/5/testing/


def test_version_displays_library_version():
    """
    Arrange/Act: Run the `version` subcommand.
    Assert: The output matches the library version.
    """
    runner: CliRunner = CliRunner()
    result: Result = runner.invoke(cli.cli, ["version"])
    assert (
        pkg_resources.require("quakesaver_application_task")[0].version
        in result.output.strip()
    ), "Version number should match library version."


def test_verbose_output():
    """
    Arrange/Act: Run the `version` subcommand with the '-v' flag.
    Assert: The output indicates verbose logging is enabled.
    """
    runner: CliRunner = CliRunner()
    result: Result = runner.invoke(cli.cli, ["-v", "version"])
    assert (
        "Verbose" in result.output.strip()
    ), "Verbose logging should be indicated in output."


def test_download_too_much_args():
    """
    Arrange/Act: Run the `download` subcommand with the '-p 50 -t 2020-01-03' flag.
    Assert: The exit code is 1.
    """
    runner: CliRunner = CliRunner()
    result: Result = runner.invoke(
        cli.cli, ["download", "-p", "50", "-d 2020-01-01 2021-01-01", "tests"]
    )
    assert result.exit_code == 2


def test_download_too_much_time():
    """
    Arrange/Act: Run the `download` subcommand with the '-f 2020-01-01 -t 2020-01-30' flag.
    Assert: The exit code is 1.
    """
    runner: CliRunner = CliRunner()
    result: Result = runner.invoke(
        cli.cli, ["download", "-d 2020-01-01 2030-01-01", "tests"]
    )
    assert result.exit_code == 2


# def test_download_past_days():
#     """
#     Arrange/Act: Run the `download` subcommand with the '-p 50' flag.
#     Assert: The output indicates data is loaded.
#     """
#     runner: CliRunner = CliRunner()
#     result: Result = runner.invoke(cli.cli, ["download", "-p", "50", "tests"])
#     assert result.exit_code == 0
#
#
# def test_download_to_from():
#     """
#     Arrange/Act: Run the `download` subcommand with the '-f 2020-01-01 -t 2020-01-30' flag.
#     Assert: The output indicates data is loaded.
#     """
#     runner: CliRunner = CliRunner()
#     result: Result = runner.invoke(
#         cli.cli, ["download", "-f", "2020-01-01", "-t", "2020-01-30", "tests"]
#     )
#     assert result.exit_code == 0
